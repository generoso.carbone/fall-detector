package com.example.raccoglidati;

import java.util.Locale;

public class CSVRecord {
    private long time;
    private float ax;
    private float ay;
    private float az;
    private double a;
    private float diff;

    public CSVRecord(long time, float ax, float ay, float az, double a, float diff) {
        this.time = time;
        this.ax = ax;
        this.ay = ay;
        this.az = az;
        this.a = a;
        this.diff = diff;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public float getAx() {
        return ax;
    }

    public void setAx(float ax) {
        this.ax = ax;
    }

    public float getAy() {
        return ay;
    }

    public void setAy(float ay) {
        this.ay = ay;
    }

    public float getAz() {
        return az;
    }

    public void setAz(float az) {
        this.az = az;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    @Override
    public String toString() {
        return String.format(Locale.ITALIAN,"%d;%f;%f;%f;%f", time, ax, ay, az, a).replaceAll(",", ".");
    }
}
