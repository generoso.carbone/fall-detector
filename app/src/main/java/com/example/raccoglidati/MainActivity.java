package com.example.raccoglidati;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements FallDetector.CSVInterface, MediaScannerConnection.MediaScannerConnectionClient {

    private static final String TAG = "MainActivity";
    private List<CSVRecord> records;
    private FallDetector fallDetector;
    private Button start;
    private Button stop;
    private Button calibrazione;
    private TextView time;
    private TextView ax;
    private TextView ay;
    private TextView az;
    private TextView a;
    private TextView diff;
    private boolean granted;
    private MediaScannerConnection mediaScannerConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        start = findViewById(R.id.start);
        stop = findViewById(R.id.stop);
        time = findViewById(R.id.time);
        calibrazione = findViewById(R.id.calibrazione);
        ax = findViewById(R.id.ax);
        ay = findViewById(R.id.ay);
        az = findViewById(R.id.az);
        a = findViewById(R.id.a);
        diff = findViewById(R.id.diff);

        stop.setEnabled(false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
                setUi();
            } else{
                granted = false;
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            }
        } else{
            setUi();
        }
    }

    private void setUi(){
        granted = true;
        mediaScannerConnection = new MediaScannerConnection(this, this);
        mediaScannerConnection.connect();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == 0){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setUi();
            } else{
                Toast.makeText(this, "Permessi non sufficienti", Toast.LENGTH_SHORT).show();
                granted = false;
            }
        }
    }

    @Override
    public void insertRecord(long time, float ax, float ay, float az, double a, float diff) {
        records.add(new CSVRecord(time, ax, ay, az, a, diff));
    }

    @Override
    public void updateUi(long time, float ax, float ay, float az, double a, float diff) {
        this.time.setText(String.valueOf(time));
        this.ax.setText(String.valueOf(ax));
        this.ay.setText(String.valueOf(ay));
        this.az.setText(String.valueOf(az));
        this.a.setText(String.valueOf(a));
        this.diff.setText(String.valueOf(diff));
    }

    public void start(View view) {
        if(calibrazioneAttiva)
            return;

        if(!granted){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            }

            return;
        }

        records = new ArrayList<>();
        records.clear();

        if(fallDetector == null)
            fallDetector = new FallDetector(this);

        fallDetector.start();

        start.setEnabled(false);
        stop.setEnabled(true);
    }

    public void stop(View view) {
        if(calibrazioneAttiva)
            return;

        if(fallDetector != null)
            fallDetector.destroy();

        if(!granted){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            }

            return;
        }

        logRecords();
        float average = average();
        Log.d(TAG, "average:\t\t" + average);
        double standardDeviation = standardDeviation(average);
        Log.d(TAG, "standard Deviation:\t" + standardDeviation);

        records.clear();
        records = null;

        stop.setEnabled(false);
        start.setEnabled(true);
    }

    private double standardDeviation(float average) {
        int n = records.size();
        float sum = 0;

        for(CSVRecord r : records){
            sum += Math.pow(r.getA() - average, 2);
        }

        return Math.sqrt(sum/n);

    }

    private float average() {
        int i = records.size();
        float sum = 0;
        for(CSVRecord r : records){
            sum += r.getA();
        }

        return sum/i;
    }

    private void logRecords() {
        if(isExternalStorageReadable() && isExternalStorageWritable()){
            Log.d(TAG, "logRecords: " + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS));
            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "raccolta");
            if (!file.mkdirs()) {
                final File f = new File(file, String.format(Locale.ITALIAN, "%d_rd.csv", Calendar.getInstance().getTimeInMillis()));
                try {
                    if(!f.createNewFile())
                        Log.d(TAG, "logRecords: file non creato");

                    FileOutputStream outputStream = new FileOutputStream(f, true);
                    outputStream.write("time;ax;ay;az;a\n".getBytes());
                    for(CSVRecord r : records){
                        Log.d(TAG, r.toString());
                        outputStream.write((r.toString() + "\n").getBytes());
                    }
                    outputStream.flush();
                    outputStream.close();

                    if(mediaScannerConnection.isConnected()){
                        mediaScannerConnection.scanFile(f.getAbsolutePath(), "text/csv");
                    } else{
                        Log.d(TAG, "logRecords: not connected");
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        }
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }

    @Override
    public void onMediaScannerConnected() {
        Log.d(TAG, "onMediaScannerConnected: check");
    }

    @Override
    public void onScanCompleted(String path, Uri uri) {
        Log.d(TAG, "onScanCompleted: path: " + path);
        Log.d(TAG, "onScanCompleted: uri: " + uri.toString());

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_STREAM,  uri);
        sendIntent.setType("*/*");
        startActivity(Intent.createChooser(sendIntent, "Invialo a generoso, pls"));
    }

    private boolean calibrazioneAttiva = false;

    public void avviaCalibrazione(View view) {
        if(calibrazioneAttiva){
            calibrazioneAttiva = false;
            calibrazione.setText("Avvia calibrazione");


            fallDetector.calibra(false);

        } else {
            calibrazioneAttiva = true;
            calibrazione.setText("Ferma calibrazione");

            if(fallDetector == null)
                fallDetector = new FallDetector(this);

            fallDetector.calibra(true);
        }
    }
}
