package com.example.raccoglidati;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener2;
import android.hardware.SensorManager;
import android.util.Log;

import java.util.Calendar;
import java.util.Locale;

import static android.content.Context.SENSOR_SERVICE;

public class FallDetector implements SensorEventListener2 {

    private final static String TAG = "FallDetector";
    private final Context context;
    private final SensorManager sensorManager;

    private float normX;
    private float normY;
    private float normZ;
    private float calX;
    private float calY;
    private float calZ;
    private long counter;
    private boolean calibration = false;

    public void calibra(boolean b) {
        if(b) {
            this.calibration = true;
            counter = 0;
            normX = 0;
            normY = 0;
            normZ = 0;

            calX = 0;
            calY = 0;
            calZ = 0;

            start();
        } else {
            if(counter > 0) {
                normX = 0f - (calX / counter);
                normY = 0f - (calY / counter);
                normZ = 9.81f - (calZ / counter);

                Log.d(TAG, String.format(Locale.ENGLISH, "destroy: calX: %f; calY: %f; calZ: %f; counter: %d", calX, calY, calZ, counter));
                Log.d(TAG, String.format(Locale.ENGLISH, "destroy: normX: %f; normY: %f; normZ: %f; counter: %d", normX, normY, normZ, counter));
            }
            counter = 0;
            this.calibration = false;

            if(sensorManager != null) {
                sensorManager.unregisterListener(this);
            }
        }
    }

    public interface CSVInterface{
        void insertRecord(long time, float ax, float ay, float az, double a, float diff);
        void updateUi(long time, float ax, float ay, float az, double a, float diff);
    }

    public FallDetector(Context context){
        this.context = context;
        sensorManager = (SensorManager) context.getSystemService(SENSOR_SERVICE);
    }

    public void start(){
        if(sensorManager != null) {
            Sensor mySensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            if(mySensor != null)
                sensorManager.registerListener(this, mySensor, SensorManager.SENSOR_DELAY_NORMAL);
            else
                Log.e(TAG, "no sensor: TYPE_ACCELEROMETER");
        } else
            Log.e(TAG, "no sensor manager: TYPE_ACCELEROMETER");
    }

    public void destroy(){
        if(sensorManager != null) {
            sensorManager.unregisterListener(this);
        }
    }
    @Override
    public void onFlushCompleted(Sensor sensor) {
        Log.d(TAG, "onFlushCompleted: " + sensor.getStringType());

    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        double rootSquare;
        float ax = event.values[0];
        float ay = event.values[1];
        float az = event.values[2];

        if(!calibration) {
            float fax = ax + normX;
            float fay = ay + normY;
            float faz = az + normZ;

            Log.d(TAG, String.format(Locale.ENGLISH, "destroy: normX: %f; normY: %f; normZ: %f; counter: %d", normX, normY, normZ, counter));
            Log.d(TAG, String.format(Locale.ENGLISH, "onSensorChanged: ax: %f; ay: %f; az: %f;", ax, ay, az));
            Log.d(TAG, String.format(Locale.ENGLISH, "onSensorChanged: fax: %f; fay: %f; faz: %f;\n", fax, fay, faz));

            rootSquare = Math.sqrt(Math.pow(fax, 2) + Math.pow(fay, 2) + Math.pow(faz, 2));
            float diff = fax + fay + faz - (float) rootSquare;
            if (context instanceof CSVInterface) {
                ((CSVInterface) context).insertRecord(
                        Calendar.getInstance().getTimeInMillis(),
                        fax,
                        fay,
                        faz,
                        rootSquare,
                        diff
                );

                ((CSVInterface) context).updateUi(
                        Calendar.getInstance().getTimeInMillis(),
                        fax,
                        fay,
                        faz,
                        rootSquare,
                        diff
                );
            }
        } else {
            calX += ax;
            calY += ay;
            calZ += az;
            counter += 1;

            Log.d(TAG, String.format(Locale.ENGLISH, "destroy: calX: %f; calY: %f; calZ: %f;", ax, ay, az));
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
